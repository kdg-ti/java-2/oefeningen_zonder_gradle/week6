package be.kdg.laptop;

import java.util.Comparator;

public class PrijsComparator implements Comparator<Laptop> {
    @Override
    public int compare(Laptop eerste, Laptop tweede) {
        return Double.compare(eerste.getPrijs(), tweede.getPrijs());
    }
}
