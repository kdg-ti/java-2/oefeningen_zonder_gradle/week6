package be.kdg;

import be.kdg.patterns.Categorie;
import be.kdg.patterns.CategorieDao;
import be.kdg.patterns.Product;
import be.kdg.patterns.ProductDao;

import java.math.BigDecimal;

/**
 * De situatie is als volgt:
 * Elk product behoort tot één bepaalde categorie. Er is dus een 1 op veel-relatie tussen Categorie en Product.
 * Werk eerst de klasse ProductDao uit, daarna de klasse CategorieDao.
 * De verwachte output vind je onderaan in commentaar.
 */
public class TestDao2 {
    public static void main(String[] args) {
        ProductDao productDao = new ProductDao();
        CategorieDao categorieDao = new CategorieDao(productDao);

        categorieDao.clear();
        productDao.clear();

        Categorie fruit = new Categorie("Fruit");
        Product bananen = new Product("Bananen", 1.69);
        productDao.create(bananen);
        fruit.voegProductToe(bananen);
        Product appelen = new Product("Appelen", 2.29);
        productDao.create(appelen);
        fruit.voegProductToe(appelen);
        System.out.println("voor toevoegen aan de database:");
        System.out.println(fruit);

        categorieDao.create(fruit);

        System.out.println("\nna ophalen uit de database:");
        fruit = categorieDao.retrieve(fruit.getId());
        System.out.println(fruit);

        Product peren = new Product("Peren", 2.99);
        productDao.create(peren);
        fruit.voegProductToe(peren);
        categorieDao.update(fruit);

        System.out.println("\nna koppelen nieuw product (peren):");
        fruit = categorieDao.retrieve(fruit.getId());
        System.out.println(fruit);

        fruit.verwijderProduct(peren);
        categorieDao.update(fruit);

        System.out.println("\nna verwijderen product (peren):");
        fruit = categorieDao.retrieve(fruit.getId());
        System.out.println(fruit);

        categorieDao.delete(fruit.getId());
        System.out.println("\nna verwijderen categorie (fruit):");
        fruit = categorieDao.retrieve(fruit.getId());
        System.out.println(fruit);

        bananen = productDao.retrieve(bananen.getId());
        System.out.println("\nop zoek naar de bananen:");
        System.out.println(bananen);

        categorieDao.close();
        productDao.close();
    }
}
/* Verwachte afdruk:
    voor toevoegen aan de database:
    categorie[-1, Fruit]
    	product[ 0, Bananen, €1,69, cat: -1]
    	product[ 1, Appelen, €2,29, cat: -1]

    na ophalen uit de database:
    categorie[0, Fruit]
    	product[ 0, Bananen, €1,69, cat: 0]
    	product[ 1, Appelen, €2,29, cat: 0]

    na koppelen nieuw product (peren):
    categorie[0, Fruit]
    	product[ 0, Bananen, €1,69, cat: 0]
    	product[ 1, Appelen, €2,29, cat: 0]
    	product[ 2, Peren  , €2,99, cat: 0]

    na verwijderen product (peren):
    categorie[0, Fruit]
    	product[ 0, Bananen, €1,69, cat: 0]
    	product[ 1, Appelen, €2,29, cat: 0]

    na verwijderen categorie (fruit):
    null

    op zoek naar de bananen:
    null
*/